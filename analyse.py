import pandas as pd
import numpy as np
from scipy.stats import mode
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from fancyimpute import MICE, NuclearNormMinimization, KNN
from sklearn.cross_validation import KFold
import re


df = pd.read_csv('input/train.csv')

# Grab title from passenger names
# full$Title <- gsub('(.*, )|(\\..*)', '', full$Name)
#
# # Show title counts by sex
# table(full$Sex, full$Title)
#
# rare_title <- c('Dona', 'Lady', 'the Countess','Capt', 'Col', 'Don',
#                 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer')
#
# full$Title[full$Title == 'Mlle']        <- 'Miss'
# full$Title[full$Title == 'Ms']          <- 'Miss'
# full$Title[full$Title == 'Mme']         <- 'Mrs'
# full$Title[full$Title %in% rare_title]  <- 'Rare Title'
df['Title'] = df['Name'].apply(lambda x: re.sub('(.*, )|(\\..*)','',x))
rare_title = ['Dona', 'Lady', 'the Countess','Capt', 'Col', 'Don','Dr', 'Major', 'Rev', 'Sir', 'Jonkheer']
df['Title'][df['Title'] == 'Mlle'] = 'Miss'
df['Title'][df['Title'] == 'Ms'] = 'Miss'
df['Title'][df['Title'] == 'Mme'] = 'Mrs'
df['Title'][df['Title'].isin(rare_title)] = 'Rare Title'

df['Ticket_no'] = df['Ticket'].apply(lambda x: max([-1]+ [int(z) for z in re.findall('\d+', x)]))  # ticket no

df = df.drop(['Name', 'Ticket', 'Cabin'], axis=1)

age_mean = df['Age'].mean()
df['Age'] = df['Age'].fillna(age_mean)

from scipy.stats import mode

mode_embarked = df['Embarked'].mode()[0][0]
df['Embarked'] = df['Embarked'].fillna(mode_embarked)

df['Gender'] = df['Sex'].map({'female': 0, 'male': 1}).astype(int)
df = pd.concat([df, pd.get_dummies(df['Embarked'], prefix='Embarked'), pd.get_dummies(df['Title'], prefix='Title')], axis=1)

df = df.drop(['Sex', 'Embarked', 'Title'], axis=1)

cols = df.columns.tolist()
cols = [cols[1]] + cols[0:1] + cols[2:]
df = df[cols]

train_data = df.values

parameter_grid = {
    'max_features': [0.5, 1.],
    'max_depth': [5., None]
}

grid_search = GridSearchCV(RandomForestClassifier(n_estimators = 1000), parameter_grid,
                            cv=5, verbose=3, n_jobs=8)

grid_search.fit(train_data[0:,2:], train_data[0:,0])

sorted(grid_search.grid_scores_, key=lambda x: x.mean_validation_score)
grid_search.best_score_
grid_search.best_params_

model = RandomForestClassifier(n_estimators = 1000, max_depth=None, max_features=0.5, n_jobs=-1)
model = model.fit(train_data[0:,2:],train_data[0:,0])

df_test = pd.read_csv('input/test.csv')

df_test['Title'] = df_test['Name'].apply(lambda x: re.sub('(.*, )|(\\..*)','',x))
rare_title = ['Dona', 'Lady', 'the Countess','Capt', 'Col', 'Don','Dr', 'Major', 'Rev', 'Sir', 'Jonkheer']
df_test['Title'][df_test['Title'] == 'Mlle'] = 'Miss'
df_test['Title'][df_test['Title'] == 'Ms'] = 'Miss'
df_test['Title'][df_test['Title'] == 'Mme'] = 'Mrs'
df_test['Title'][df_test['Title'].isin(rare_title)] = 'Rare Title'

df_test['Ticket_no'] = df_test['Ticket'].apply(lambda x: max([-1]+ [int(z) for z in re.findall('\d+', x)]))  # ticket no

df_test = df_test.drop(['Name', 'Ticket', 'Cabin'], axis=1)

df_test['Age'] = df_test['Age'].fillna(age_mean)

fare_means = df.pivot_table('Fare', index='Pclass', aggfunc='mean')
df_test['Fare'] = df_test[['Fare', 'Pclass']].apply(lambda x:
                            fare_means[x['Pclass']] if pd.isnull(x['Fare'])
                            else x['Fare'], axis=1)

df_test['Gender'] = df_test['Sex'].map({'female': 0, 'male': 1}).astype(int)
df_test = pd.concat([df_test, pd.get_dummies(df_test['Embarked'], prefix='Embarked'), pd.get_dummies(df_test['Title'], prefix='Title')],
                axis=1)

df_test = df_test.drop(['Sex', 'Embarked', 'Title'], axis=1)

test_data = df_test.values

output = model.predict(test_data[:,1:])


result = np.c_[test_data[:,0].astype(int), output.astype(int)]

df_result = pd.DataFrame(result[:,0:2], columns=['PassengerId', 'Survived'])
df_result.to_csv('results/titanic_1-extra_title-ticket_no.csv', index=False)

X = train_data[:, 2:]
y = train_data[:, 0]

n = len(df)/2


cv = KFold(n=len(train_data), n_folds=2)

for training_set, test_set in cv:
    X_train = X[training_set]
    y_train = y[training_set]
    X_test = X[test_set]
    y_test = y[test_set]
    model = RandomForestClassifier(n_estimators=1000)
    model.fit(X_train, y_train)
    y_prediction = model.predict(X_test)
    print("prediction accuracy:", np.sum(y_test == y_prediction)*1./len(y_test))

from sklearn.svm import SVC

model = SVC(kernel='linear')
model = model.fit(train_data[0:,2:], train_data[0:,0])

output = model.predict(test_data[:,1:])

result = np.c_[test_data[:,0].astype(int), output.astype(int)]

df_result = pd.DataFrame(result[:,0:2], columns=['PassengerId', 'Survived'])
df_result.to_csv('results/titanic_2-1.csv', index=False)


parameter_grid = {
    'C': [1., 10.],
    'gamma': [0.1, 1.]
}

grid_search = GridSearchCV(SVC(kernel='linear'), parameter_grid, cv=5, verbose=3, n_jobs=8)
grid_search.fit(train_data[0:,2:], train_data[0:,0])

sorted(grid_search.grid_scores_, key=lambda x: x.mean_validation_score)
grid_search.best_score_
grid_search.best_params_

model = SVC(kernel='linear', C=1.0, gamma=0.1)
model = model.fit(train_data[0:,2:], train_data[0:,0])

output = model.predict(test_data[:,1:])

result = np.c_[test_data[:,0].astype(int), output.astype(int)]

df_result = pd.DataFrame(result[:,0:2], columns=['PassengerId', 'Survived'])
df_result.to_csv('results/titanic_2-rbf.csv', index=False)

import autosklearn.classification

df = pd.read_csv('input/train.csv')

df = df.drop(['Name', 'Ticket', 'Cabin'], axis=1)
df['Gender'] = df['Sex'].map({'female': 0, 'male': 1}).astype(int)
df = pd.concat([df, pd.get_dummies(df['Embarked'], prefix='Embarked')], axis=1)

df = df.drop(['Sex', 'Embarked'], axis=1)

cols = df.columns.tolist()
cols = [cols[1]] + cols[0:1] + cols[2:]
df = df[cols]

train_data = df.values

df_test = pd.read_csv('input/test.csv')

df_test = df_test.drop(['Name', 'Ticket', 'Cabin'], axis=1)

df_test['Gender'] = df_test['Sex'].map({'female': 0, 'male': 1}).astype(int)
df_test = pd.concat([df_test, pd.get_dummies(df_test['Embarked'], prefix='Embarked')],
                axis=1)

df_test = df_test.drop(['Sex', 'Embarked'], axis=1)

test_data = df_test.values

cls = autosklearn.classification.AutoSklearnClassifier()
x_train = np.ascontiguousarray(train_data[0:, 2:])
y_train = train_data[0:, 0]
cls.fit(x_train, y_train)
x_test = np.ascontiguousarray(test_data[:,1:])

predictions = cls.predict(test_data[:,1:])

result = np.c_[test_data[:,0].astype(int), predictions.astype(int)]

df_result = pd.DataFrame(result[:,0:2], columns=['PassengerId', 'Survived'])
df_result.to_csv('results/titanic_2-autosk-all.csv', index=False)



from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_iris
from sklearn.ensemble import AdaBoostClassifier